class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :load_manifest!

  private

  def load_manifest!
    # https://github.com/ztoben/assets-webpack-plugin#using-this-with-rails
    path = Rails.root.join('public', 'assets', 'manifest.json')
    file = File.read(path)
    json = JSON.parse(file)

    @scripts = []
    @styles = []

    name = controller_name && json[controller_name] ? controller_name : 'default'
    @scripts << json[name]['js']
    @styles << json[name]['css']
  end
end
