import { library } from '@fortawesome/fontawesome-svg-core'
export * from '@fortawesome/free-solid-svg-icons'

// pass icons imported from @fortawesome/free-solid-svg-icons
export function useIcons(...icons) {
  library.add(...icons)
}
