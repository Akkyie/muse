import '../lib/rails'
import '../lib/style'
import { useIcons, faPlus, faSignOutAlt } from '../lib/icons'

useIcons(faPlus, faSignOutAlt)
