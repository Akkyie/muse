import '../lib/shared'
import { useIcons, faArrowLeft, faEdit } from '../lib/icons'

useIcons(faArrowLeft, faEdit)
