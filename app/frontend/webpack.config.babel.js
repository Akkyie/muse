import path from 'path'
import fs from 'fs'
import Sass from 'sass'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import AssetsPlugin from 'assets-webpack-plugin'
import TerserPlugin from 'terser-webpack-plugin'
import CleanWebpackPlugin from 'clean-webpack-plugin'

const OUTPUT_ROOT = path.resolve(__dirname, '../../public/assets')
const SCRIPTS_ROOT = path.join(__dirname, 'scripts')
const STYLES_ROOT = path.join(__dirname, 'styles')

const files = root =>
  fs
    .readdirSync(root, { withFileTypes: true })
    .filter(dirent => !dirent.isDirectory())
    .map(dirent => [path.parse(dirent.name).name, path.join(root, dirent.name)])

const entries = [...files(SCRIPTS_ROOT), ...files(STYLES_ROOT)].reduce(
  (obj, [name, path]) => ({ ...obj, [name]: [...(obj[name] || []), path] }),
  {}
)

export default {
  entry: entries,

  output: {
    path: OUTPUT_ROOT,
    filename: '[name]-[hash].js',
    publicPath: '/assets'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        ]
      },
      {
        test: /\.s[ca]ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              implementation: Sass
            }
          }
        ]
      }
    ]
  },

  optimization: {
    minimizer: [new TerserPlugin()]
  },

  plugins: [
    new CleanWebpackPlugin(OUTPUT_ROOT, { allowExternal: true }),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      publicPath: '/assets'
    }),
    new AssetsPlugin({
      path: OUTPUT_ROOT,
      filename: 'manifest.json',
      publicPath: '/assets'
    })
  ]
}
