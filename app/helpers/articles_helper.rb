require 'uri'

module ArticlesHelper
  def iframe_url_for(url)
    URI.join(request.url, url)
  end
end
