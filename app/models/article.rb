class Article < ApplicationRecord
  has_one_attached :pdf_file

  def tag_text=(val)
    val = val
      .split(",")
      .map { |tag| tag.strip }
      .select { |tag| not tag.empty? }
      .map { |tag| "#{tag}," }
      .join
    super(val.to_s.strip)
  end

  def tag_list
    if tag_text
      tag_text.split(",")
    else
      []
    end
  end
end
