Rails.application.routes.draw do
  root 'articles#index'

  resources :articles do
    get 'search/:tag', to: 'articles#search', as: 'search', on: :collection
  end

  devise_for :users
end
