class AddTagTextToArticle < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :tag_text, :string
  end
end
